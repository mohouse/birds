package com.example.demo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Builder
public class Bird {

  @Id
  @GeneratedValue
  private Long id;

  @Version
  @JsonIgnore
  private Long version;

  @Column(unique = true)
  private String name;

}
