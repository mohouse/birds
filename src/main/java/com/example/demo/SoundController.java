package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Random;

@Controller
public class SoundController {

    private final SoundRepository soundRepository;

    public SoundController(SoundRepository soundRepository) {
        this.soundRepository = soundRepository;
    }

    @RequestMapping(value = "/sound/{name}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public HttpEntity<byte[]> getSound(@PathVariable String name) {
        System.out.println("Loading sound: " + name);
        List<Sound> sounds = soundRepository.findByBird(name);
        System.out.println("Loaded sounds: " + sounds.size());

        Sound sound = sounds.get(new Random().nextInt(sounds.size()));

        System.out.println("Loaded sound: " + sound.getBird().getName());
        HttpHeaders header = new HttpHeaders();
        header.setContentType(new MediaType("audio", "mpeg"));
        header.setContentLength(sound.getSound().length);
        return new HttpEntity<>(sound.getSound(), header);
    }
}