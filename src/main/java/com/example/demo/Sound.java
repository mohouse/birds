package com.example.demo;

import lombok.Builder;
import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Builder
public class Sound {

  @Id
  @GeneratedValue
  private Long id;

  @Lob
  private byte[] sound;

  @ManyToOne
  private Bird bird;
}
