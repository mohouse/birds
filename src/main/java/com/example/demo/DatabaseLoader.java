package com.example.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

@Component
public class DatabaseLoader implements CommandLineRunner {

    private final BirdRepository birdRepository;
    private final SoundRepository soundRepository;

    public DatabaseLoader(BirdRepository birdRepository, SoundRepository soundRepository) {
        this.birdRepository = birdRepository;
        this.soundRepository = soundRepository;
    }

    @Override
    public void run(String... strings) throws IOException {
        try (Stream<Path> paths = Files.walk(Paths.get(getSoundDirectory()))) {
            paths
                    .filter(Files::isRegularFile)
                    .filter(path -> path.toString().endsWith(".mp3"))
                    .forEach(path -> handleSound(path.getFileName()));
        }
    }

    private String getSoundDirectory() {
        return "src" + File.separator + "main" + File.separator + "resources"
                + File.separator + "sounds";
    }

    private void handleSound(Path path) {
        String nameOfSound = path.toString().substring(0, path.toString().indexOf("."));
        String name = getNameForSound(nameOfSound);
        List<Bird> birds = birdRepository.findByName(name);
        Bird bird;
        if (birds.isEmpty()) {
            bird = this.birdRepository.save(Bird.builder().name(name).build());
        } else {
            bird = birds.get(0);
        }
        this.soundRepository.save(Sound.builder().sound(getSound(path)).bird(bird).build());
    }

    private byte[] getSound(Path path) {
        try {
            return Files.readAllBytes(Paths.get(getSoundDirectory() + File.separator + path.getFileName()));
        } catch (IOException e) {
            return null;
        }
    }

    private String getNameForSound(String nameOfSound) {
        if (nameOfSound.contains("_")) {
            return nameOfSound.substring(0, nameOfSound.indexOf("_"));
        }

        return nameOfSound;
    }
}