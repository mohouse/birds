package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SoundRepository extends JpaRepository<Sound, Long> {

    @Query("SELECT s FROM Sound s WHERE s.bird.name = ?1")
    List<Sound> findByBird(String bird);
}
