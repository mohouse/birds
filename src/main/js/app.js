const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');
const follow = require('./follow');

const Sound = require('react-sound').default;

const root = '/api';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            birds: [],
            randomSound: "",
            playStatus: Sound.status.PLAYING,
            result: "",
            solution: ""
        };
        this.handleSongFinishedPlaying = this.handleSongFinishedPlaying.bind(this);
        this.playAgain = this.playAgain.bind(this);
        this.playAnother = this.playAnother.bind(this);
        this.onSelected = this.onSelected.bind(this);
        this.onPlay = this.onPlay.bind(this);
        this.cheat = this.cheat.bind(this);
    }

    loadFromServer() {
        follow(client, root, [
            {rel: 'birds', params: {size: 100}}]
        ).then(birdsCollection => {
            return client({
                method: 'GET',
                path: birdsCollection.entity._links.profile.href,
                headers: {'Accept': 'application/schema+json'}
            }).then(schema => {
                this.schema = schema.entity;
                return birdsCollection;
            });
        }).done(birdsCollection => {
            var birds = birdsCollection.entity._embedded.birds;
            birds = birds.sort((a, b) => a.name > b.name);
            var randomBird = birds[Math.floor(Math.random() * birds.length)];
            this.setState({
                birds: birds,
                randomSound: randomBird.name
            });
        });
    }

    componentDidMount() {
        this.loadFromServer();
    }

    playAgain() {
        this.setState({
            playStatus: Sound.status.PLAYING
        });
    }

    cheat() {
        this.setState({
            solution: this.state.randomSound
        });
    }

    playAnother() {
        this.setState({
            randomSound: this.state.birds[Math.floor(Math.random() * this.state.birds.length)].name,
            playStatus: Sound.status.PLAYING,
            result: "",
            solution: ""
        });
    }

    handleSongFinishedPlaying() {
        this.setState({
            playStatus: Sound.status.STOPPED
        });
    }

    onSelected(bird) {
        let result = "Falsch";
        if (bird === this.state.randomSound) {
            result = "Richtig";
        }
        this.setState({
            result: result
        });
    }

    onPlay(bird) {
        this.setState({
            randomSound: bird,
            playStatus: Sound.status.PLAYING
        });
    }

    render() {
        return (
            <div>
                Deine Lösung ist: {this.state.result}
                <br />
                Lösung: {this.state.solution}
                <RandomSound sound={this.state.randomSound}
                             playStatus={this.state.playStatus}
                             playAgain={this.playAgain}
                             playAnother={this.playAnother}
                             cheat={this.cheat}
                             handleSongFinishedPlaying={this.handleSongFinishedPlaying}
                />
                <BirdList birds={this.state.birds}
                          onSelected={this.onSelected}
                          onPlay={this.onPlay}
                />
                <TextWindow style={{backgroundColor: "green"}} />
            </div>
        )
    }
}

class RandomSound extends React.Component {

    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <Sound url={"sound/" + this.props.sound}
                       playStatus={this.props.playStatus}
                       playFromPosition={100 /* in milliseconds */}
                       onLoading={this.handleSongLoading}
                       onPlaying={this.handleSongPlayilistAssertionErrorng}
                       onFinishedPlaying={this.props.handleSongFinishedPlaying}/>
                <button value="nochmal" onClick={this.props.playAgain}>again</button>
                <button value="neuer Sound" onClick={this.props.playAnother}>another</button>
                <button value="cheat" onClick={this.props.cheat}>Auflösen</button>
            </div>
        )
    }
}

class TextWindow extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div style={backgroundColor: "red"}>
                Deine Lösung ist: {this.props.result}
                <br />
                Richtig wäre: {this.props.solution}
            </div>
        )
    }
}


class BirdList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var birds = this.props.birds.map(bird =>
            <Bird key={bird._links.self.href}
                  bird={bird}
                  onSelected={this.props.onSelected}
                  onPlay={this.props.onPlay}
            />
        );

        return (
            <div>
                <table>
                    <tbody>
                    <tr>
                        <th>Name</th>
                        <th></th>
                        <th></th>
                    </tr>
                    {birds}
                    </tbody>
                </table>
            </div>
        )
    }
}

class Bird extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <tr>
                <td>{this.props.bird.name}</td>
                <td>
                    <button value="Ich bins" onClick={() => this.props.onSelected(this.props.bird.name)}>It's me
                    </button>
                </td>
                <td>
                    <button value="playme" onClick={() => this.props.onPlay(this.props.bird.name)}>Play me
                    </button>
                </td>
            </tr>
        )
    }
}

ReactDOM.render(
    <App/>,
    document.getElementById('react')
);